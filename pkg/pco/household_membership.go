package pco

import (
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
)

var (
	_ jsonapi.Unmarshalable = &HouseholdMembership{}
	_ jsonapi.Marshalable   = &HouseholdMembership{}
)

func (*HouseholdMembership) GetType() string {
	return "HouseholdMembership"
}

func (h *HouseholdMembership) SetId(s string) error {
	if s != "" {
		h.Id = String(s)
	}

	return nil
}

func (h *HouseholdMembership) GetId() (*string, error) {
	return h.Id, nil
}

func (h *HouseholdMembership) relations() []relationProperty {
	return []relationProperty{{
		many:    false,
		key:     "person",
		field:   "Person",
		models:  h,
		present: h.Person != nil,
	}}
}

func (h *HouseholdMembership) SetRelationships(
	doc *jsonapi.Document,
	docMap *jsonapi.IncludedMap,
) error {
	return setRelationships(doc, docMap, h.relations())
}

func (h *HouseholdMembership) GetRelationships() (map[string]*jsonapi.Payload, error) {
	payloads := map[string]*jsonapi.Payload{}
	if err := getRelationships(payloads, h.relations()); err != nil {
		return nil, err
	}
	return payloads, nil
}
