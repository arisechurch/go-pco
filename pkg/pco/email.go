package pco

import (
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
)

var (
	_ jsonapi.Unmarshalable = &Email{}
	_ jsonapi.Marshalable   = &Email{}
)

func (*Email) GetType() string {
	return "Email"
}

func (e *Email) SetId(s string) error {
	if s != "" {
		e.Id = String(s)
	}
	return nil
}

func (e *Email) GetId() (*string, error) {
	return e.Id, nil
}

func (e *Email) SetRelationships(
	doc *jsonapi.Document,
	docMap *jsonapi.IncludedMap,
) error {
	// Field Definition
	payload := doc.Relationships["person"]
	if payload != nil {
		person := &Person{}
		if err := payload.UnmarshalOneRelation(
			docMap,
			person,
		); err != nil {
			return err
		}
		e.Person = person
	}
	return nil
}

func (e *Email) GetRelationships() (map[string]*jsonapi.Payload, error) {
	payloads := map[string]*jsonapi.Payload{}

	// Field Definition
	if e.Person != nil {
		payload, err := jsonapi.NewPayloadOne(
			e.Person,
			false,
		)
		if err != nil {
			return nil, err
		}
		payloads["field_definition"] = payload
	}

	return payloads, nil
}
