package pco

import (
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
)

var (
	_ jsonapi.Unmarshalable = &Person{}
	_ jsonapi.Marshalable   = &Person{}
)

func (p *Person) GetType() string {
	return "Person"
}

func (p *Person) SetId(s string) error {
	if s != "" {
		p.Id = String(s)
	}

	return nil
}

func (p *Person) GetId() (*string, error) {
	return p.Id, nil
}

func (p *Person) relations() []relationProperty {
	return []relationProperty{{
		many:    true,
		key:     "emails",
		models:  &p.Emails,
		present: len(p.Emails) > 0,
	}, {
		many:    true,
		key:     "households",
		models:  &p.Households,
		present: len(p.Households) > 0,
	}, {
		many:    true,
		key:     "field_data",
		models:  &p.FieldData,
		present: len(p.FieldData) > 0,
	}, {
		many:    true,
		key:     "person_apps",
		models:  &p.PersonApps,
		present: len(p.PersonApps) > 0,
	}, {
		many:    true,
		key:     "phone_numbers",
		models:  &p.PhoneNumbers,
		present: len(p.PhoneNumbers) > 0,
	}}
}

func (p *Person) SetRelationships(
	doc *jsonapi.Document,
	docMap *jsonapi.IncludedMap,
) error {
	return setRelationships(doc, docMap, p.relations())
}

func (p *Person) GetRelationships() (map[string]*jsonapi.Payload, error) {
	payloads := map[string]*jsonapi.Payload{}
	if err := getRelationships(payloads, p.relations()); err != nil {
		return nil, err
	}
	return payloads, nil
}

func (p *Person) FieldDataValue(defId string) (*FieldDatum, int) {
	if p.FieldData == nil {
		return nil, -1
	}

	for i, data := range p.FieldData {
		if data.FieldDefinition == nil || data.Value == nil {
			continue
		}

		if data.FieldDefinition.Id != nil &&
			*data.FieldDefinition.Id == defId {
			return data, i
		}
	}

	return nil, -1
}

func (p *Person) RemoveFieldData(index int) {
	copy(p.FieldData[index:], p.FieldData[index+1:])
	p.FieldData[len(p.FieldData)-1] = nil
	p.FieldData = p.FieldData[:len(p.FieldData)-1]
}
