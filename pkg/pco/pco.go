package pco

import (
	"time"
)

type Household struct {
	Id                 *string    `json:"-"`
	Name               *string    `json:"name,omitempty"`
	MemberCount        *int       `json:"member_count,omitempty"`
	PrimaryContactId   *string    `json:"primary_contact_id,omitempty"`
	PrimaryContactName *string    `json:"primary_contact_name,omitempty"`
	CreatedAt          *time.Time `json:"created_at,omitempty"`
	UpdatedAt          *time.Time `json:"updated_at,omitempty"`

	HouseholdMemberships []*HouseholdMembership `json:"-"`
	People               []*Person              `json:"-"`
	PrimaryContact       *Person                `json:"-"`
}

type HouseholdMembership struct {
	Id         *string `json:"-"`
	Pending    *bool   `json:"pending,omitempty"`
	PersonName *string `json:"person_name,omitempty"`

	Person *Person `json:"-"`
}

type List struct {
	Id                   *string    `json:"-"`
	Name                 *string    `json:"name,omitempty"`
	Description          *string    `json:"description,omitempty"`
	Subset               *string    `json:"subset,omitempty"`
	Status               *string    `json:"status,omitempty"`
	Returns              *string    `json:"returns,omitempty"`
	ReturnOriginalIfNone *bool      `json:"return_original_if_none,omitempty"`
	HasInactiveResults   *bool      `json:"has_inactive_results,omitempty"`
	TotalPeople          *int       `json:"total_people,omitempty"`
	BatchCompletedAt     *time.Time `json:"batch_completed_at,omitempty"`
	IncludeInactive      *bool      `json:"include_inactive,omitempty"`
	CreatedAt            *time.Time `json:"created_at,omitempty"`
	UpdatedAt            *time.Time `json:"updated_at,omitempty"`

	People []*Person `json:"-"`
}

type Person struct {
	Id                *string    `json:"-"`
	Status            *string    `json:"status,omitempty"`
	Name              *string    `json:"name,omitempty"`
	FirstName         *string    `json:"first_name,omitempty"`
	MiddleName        *string    `json:"middle_name,omitempty"`
	LastName          *string    `json:"last_name,omitempty"`
	GivenName         *string    `json:"given_name,omitempty"`
	GoesByName        *string    `json:"goes_by_name,omitempty"`
	Nickname          *string    `json:"nickname,omitempty"`
	Gender            *string    `json:"gender,omitempty"`
	Birthdate         *string    `json:"birthdate,omitempty"`
	Anniversary       *string    `json:"anniversary,omitempty"`
	Avatar            *string    `json:"avatar,omitempty"`
	MedicalNotes      *string    `json:"medical_notes,omitempty"`
	Child             *bool      `json:"child,omitempty"`
	Membership        *string    `json:"membership,omitempty"`
	SchoolType        *string    `json:"school_type,omitempty"`
	Grade             *int       `json:"grade,omitempty"`
	GraduationYear    *int       `json:"graduation_year,omitempty"`
	PeoplePermissions *string    `json:"people_permissions,omitempty"`
	SiteAdministrator *bool      `json:"site_administrator,omitempty"`
	RemoteId          *string    `json:"remote_id,omitempty"`
	CreatedAt         *time.Time `json:"created_at,omitempty"`
	UpdatedAt         *time.Time `json:"updated_at,omitempty"`

	Emails       []*Email       `json:"-"`
	FieldData    []*FieldDatum  `json:"-"`
	Households   []*Household   `json:"-"`
	PhoneNumbers []*PhoneNumber `json:"-"`
	PersonApps   []*PersonApp   `json:"-"`
}

type Email struct {
	Id       *string `json:"-"`
	Address  *string `json:"address,omitempty"`
	Location *string `json:"location,omitempty"`
	Primary  *bool   `json:"primary,omitempty"`

	Person *Person `json:"-"`
}

type App struct {
	Id   *string `json:"-"`
	Name *string `json:"name,omitempty"`
	Url  *string `json:"url,omitempty"`
}

type PersonApp struct {
	Id                *string `json:"-"`
	AllowPcoLogin     *bool   `json:"allow_pco_login,omitempty"`
	PeoplePermissions *string `json:"people_permissions,omitempty"`

	App    *App    `json:"-"`
	Person *Person `json:"-"`
}

type Customizable struct {
	Id *string `json:"id"`
}

type FieldDatum struct {
	Id              *string `json:"-"`
	File            *File   `json:"file,omitempty"`
	FileContentType *string `json:"file_content_type,omitempty"`
	FileName        *string `json:"file_name,omitempty"`
	FileSize        *int    `json:"file_size,omitempty"`
	Value           *string `json:"value,omitempty"`

	FieldDefinition *FieldDefinition `json:"-"`
	Customizable    *Customizable    `json:"-"`
}

type File struct {
	Url *string `json:"url"`
}

type FieldDefinition struct {
	Id        *string    `json:"-"`
	Name      *string    `json:"name,omitempty"`
	Slug      *string    `json:"slug,omitempty"`
	Config    *string    `json:"config,omitempty"`
	DataType  *string    `json:"data_type,omitempty"`
	Sequence  *int       `json:"sequence,omitempty"`
	DeletedAt *time.Time `json:"deleted_at,omitempty"`
}

type PhoneNumber struct {
	Id        *string    `json:"-"`
	Carrier   *string    `json:"carrier,omitempty"`
	Number    *string    `json:"number,omitempty"`
	Location  *string    `json:"location,omitempty"`
	Primary   *bool      `json:"primary,omitempty"`
	CreatedAt *time.Time `json:"created_at,omitempty"`
	UpdatedAt *time.Time `json:"updated_at,omitempty"`

	Person *Person `json:"-"`
}

type Pass struct {
	Id        *string    `json:"-"`
	Code      *string    `json:"code,omitempty"`
	Kind      *string    `json:"kind,omitempty"`
	CreatedAt *time.Time `json:"created_at,omitempty"`
	UpdatedAt *time.Time `json:"updated_at,omitempty"`

	Person *Person `json:"-"`
}

// Request options
type Options struct {
	Include string `url:"include,omitempty"`
	Filter  string `url:"filter,omitempty"`
	Order   string `url:"order,omitempty"`

	PerPage int `url:"per_page,omitempty"`
	Offset  int `url:"offset,omitempty"`
}

// Internal

type relationProperty struct {
	many    bool
	key     string
	field   string
	models  interface{}
	present bool
}
