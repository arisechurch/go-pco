package pco

import (
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
)

var (
	_ jsonapi.Unmarshalable = &FieldDefinition{}
	_ jsonapi.Marshalable   = &FieldDefinition{}
)

func (*FieldDefinition) GetType() string {
	return "FieldDefinition"
}

func (f *FieldDefinition) SetId(s string) error {
	if s != "" {
		f.Id = String(s)
	}
	return nil
}

func (f *FieldDefinition) GetId() (*string, error) {
	return f.Id, nil
}

func (f *FieldDefinition) SetRelationships(
	doc *jsonapi.Document,
	docMap *jsonapi.IncludedMap,
) error {
	return nil
}

func (f *FieldDefinition) GetRelationships() (map[string]*jsonapi.Payload, error) {
	payloads := map[string]*jsonapi.Payload{}

	return payloads, nil
}
