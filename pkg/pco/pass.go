package pco

import (
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
)

var (
	_ jsonapi.Unmarshalable = &Pass{}
	_ jsonapi.Marshalable   = &Pass{}
)

func (p *Pass) GetType() string {
	return "Pass"
}

func (p *Pass) SetId(s string) error {
	if s != "" {
		p.Id = String(s)
	}

	return nil
}

func (p *Pass) GetId() (*string, error) {
	return p.Id, nil
}

func (p *Pass) relations() []relationProperty {
	return []relationProperty{{
		many:    false,
		key:     "person",
		field:   "Person",
		models:  p,
		present: p.Person != nil,
	}}
}

func (p *Pass) SetRelationships(
	doc *jsonapi.Document,
	docMap *jsonapi.IncludedMap,
) error {
	return setRelationships(doc, docMap, p.relations())
}

func (p *Pass) GetRelationships() (map[string]*jsonapi.Payload, error) {
	payloads := map[string]*jsonapi.Payload{}
	if err := getRelationships(payloads, p.relations()); err != nil {
		return nil, err
	}
	return payloads, nil
}
