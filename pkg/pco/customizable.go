package pco

import (
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
)

var (
	_ jsonapi.Unmarshalable = &Customizable{}
	_ jsonapi.Marshalable   = &Customizable{}
)

func (*Customizable) GetType() string {
	return "Customizable"
}

func (c *Customizable) SetId(s string) error {
	if s != "" {
		c.Id = String(s)
	}
	return nil
}

func (c *Customizable) GetId() (*string, error) {
	return c.Id, nil
}

func (c *Customizable) SetRelationships(
	doc *jsonapi.Document,
	docMap *jsonapi.IncludedMap,
) error {
	return nil
}

func (c *Customizable) GetRelationships() (map[string]*jsonapi.Payload, error) {
	payloads := map[string]*jsonapi.Payload{}

	return payloads, nil
}
