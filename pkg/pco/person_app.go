package pco

import (
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
)

var (
	_ jsonapi.Unmarshalable = &PersonApp{}
	_ jsonapi.Marshalable   = &PersonApp{}
)

func (p *PersonApp) GetType() string {
	return "PersonApp"
}

func (p *PersonApp) SetId(s string) error {
	if s != "" {
		p.Id = String(s)
	}

	return nil
}

func (p *PersonApp) GetId() (*string, error) {
	return p.Id, nil
}

func (p *PersonApp) relations() []relationProperty {
	return []relationProperty{{
		many:    false,
		key:     "person",
		field:   "Person",
		models:  p,
		present: p.Person != nil,
	}, {
		many:    false,
		key:     "app",
		field:   "App",
		models:  p,
		present: p.App != nil,
	}}
}

func (p *PersonApp) SetRelationships(
	doc *jsonapi.Document,
	docMap *jsonapi.IncludedMap,
) error {
	return setRelationships(doc, docMap, p.relations())
}

func (p *PersonApp) GetRelationships() (map[string]*jsonapi.Payload, error) {
	payloads := map[string]*jsonapi.Payload{}
	if err := getRelationships(payloads, p.relations()); err != nil {
		return nil, err
	}
	return payloads, nil
}
