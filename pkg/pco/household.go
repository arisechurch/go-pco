package pco

import (
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
)

var (
	_ jsonapi.Unmarshalable = &Household{}
	_ jsonapi.Marshalable   = &Household{}
)

func (*Household) GetType() string {
	return "Household"
}

func (h *Household) SetId(s string) error {
	if s != "" {
		h.Id = String(s)
	}

	return nil
}

func (h *Household) GetId() (*string, error) {
	return h.Id, nil
}

func (h *Household) relations() []relationProperty {
	return []relationProperty{{
		many:    true,
		key:     "household_memberships",
		models:  &h.HouseholdMemberships,
		present: len(h.HouseholdMemberships) > 0,
	}, {
		many:    true,
		key:     "people",
		models:  &h.People,
		present: len(h.People) > 0,
	}, {
		many:    false,
		key:     "primary_contact",
		field:   "PrimaryContact",
		models:  h,
		present: h.PrimaryContact != nil,
	}}
}

func (h *Household) SetRelationships(
	doc *jsonapi.Document,
	docMap *jsonapi.IncludedMap,
) error {
	return setRelationships(doc, docMap, h.relations())
}

func (h *Household) GetRelationships() (map[string]*jsonapi.Payload, error) {
	payloads := map[string]*jsonapi.Payload{}
	if err := getRelationships(payloads, h.relations()); err != nil {
		return nil, err
	}
	return payloads, nil
}
func (h *Household) PeopleIds() []string {
	if h.People == nil {
		return nil
	}

	ids := []string{}
	for _, p := range h.People {
		ids = append(ids, *p.Id)
	}

	return ids
}

func (h *Household) IsMember(id string) bool {
	if h.People == nil {
		return false
	}

	for _, p := range h.People {
		if p.Id == nil {
			continue
		}
		if id == *p.Id {
			return true
		}
	}

	return false
}
