package pco

import (
	"errors"
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
	"reflect"
)

var (
	unmarshalableType reflect.Type = reflect.TypeOf((*jsonapi.Unmarshalable)(nil)).Elem()
	marshalableType   reflect.Type = reflect.TypeOf((*jsonapi.Marshalable)(nil)).Elem()
)

func setRelationships(
	doc *jsonapi.Document,
	docMap *jsonapi.IncludedMap,
	relations []relationProperty,
) error {
	for _, r := range relations {
		payload := doc.Relationships[r.key]
		if payload == nil {
			continue
		}

		if r.many {
			if err := setRelationshipMany(payload, docMap, r); err != nil {
				return err
			}
		} else {
			if err := setRelationshipOne(payload, docMap, r); err != nil {
				return err
			}
		}
	}

	return nil
}

func setRelationshipMany(
	payload *jsonapi.Payload,
	docMap *jsonapi.IncludedMap,
	r relationProperty,
) error {
	if err := payload.UnmarshalManyRelations(
		docMap,
		r.models,
	); err != nil {
		return err
	}

	return nil
}

func getRelationshipOneField(r relationProperty) (*reflect.Value, error) {
	modelPtrVal := reflect.ValueOf(r.models)
	if modelPtrVal.Kind() != reflect.Ptr {
		return nil, errors.New("setRelationshipOne r.models needs to be a pointer")
	}
	modelVal := modelPtrVal.Elem()

	fieldPtrVal := modelVal.FieldByName(r.field)
	if fieldPtrVal.Kind() != reflect.Ptr {
		return nil, errors.New("setRelationshipOne r.models field needs to be a pointer")
	}

	return &fieldPtrVal, nil
}

func setRelationshipOne(
	payload *jsonapi.Payload,
	docMap *jsonapi.IncludedMap,
	r relationProperty,
) error {
	fieldPtrVal, err := getRelationshipOneField(r)
	if err != nil {
		return err
	}
	newModel := reflect.New(fieldPtrVal.Type().Elem())

	if !newModel.Type().Implements(unmarshalableType) {
		return errors.New("setRelationshipOne r.models does not implement jsonapi.Unmarshalable")
	}
	model := newModel.Interface().(jsonapi.Unmarshalable)

	if err := payload.UnmarshalOneRelation(docMap, model); err != nil {
		return err
	}

	fieldPtrVal.Set(newModel)

	return nil
}

func getRelationships(
	payloads map[string]*jsonapi.Payload,
	relations []relationProperty,
) error {
	for _, r := range relations {
		if !r.present {
			continue
		}

		if r.many {
			if err := getRelationshipMany(payloads, r); err != nil {
				return err
			}
		} else {
			if err := getRelationshipOne(payloads, r); err != nil {
				return err
			}
		}
	}

	return nil
}

func getRelationshipMany(
	payloads map[string]*jsonapi.Payload,
	r relationProperty,
) error {
	payload, err := jsonapi.NewPayloadMany(
		r.models,
		false,
	)
	if err != nil {
		return err
	}

	payloads[r.key] = payload

	return nil
}

func getRelationshipOne(
	payloads map[string]*jsonapi.Payload,
	r relationProperty,
) error {
	fieldPtrVal, err := getRelationshipOneField(r)
	if err != nil {
		return err
	}

	if !fieldPtrVal.Type().Implements(marshalableType) {
		return errors.New("getRelationshipOne r.models does not implement jsonapi.Unmarshalable")
	}
	model := fieldPtrVal.Interface().(jsonapi.Marshalable)

	payload, err := jsonapi.NewPayloadOne(
		model,
		false,
	)
	if err != nil {
		return err
	}
	payloads[r.key] = payload

	return nil
}
