package pco

import (
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
)

var (
	_ jsonapi.Unmarshalable = &PhoneNumber{}
	_ jsonapi.Marshalable   = &PhoneNumber{}
)

func (p *PhoneNumber) GetType() string {
	return "PhoneNumber"
}

func (p *PhoneNumber) SetId(s string) error {
	if s != "" {
		p.Id = String(s)
	}
	return nil
}

func (p *PhoneNumber) GetId() (*string, error) {
	return p.Id, nil
}

func (p *PhoneNumber) SetRelationships(
	doc *jsonapi.Document,
	docMap *jsonapi.IncludedMap,
) error {
	// Field Definition
	payload := doc.Relationships["person"]
	if payload != nil {
		person := &Person{}
		if err := payload.UnmarshalOneRelation(
			docMap,
			person,
		); err != nil {
			return err
		}
		p.Person = person
	}
	return nil
}

func (p *PhoneNumber) GetRelationships() (map[string]*jsonapi.Payload, error) {
	payloads := map[string]*jsonapi.Payload{}

	// Field Definition
	if p.Person != nil {
		payload, err := jsonapi.NewPayloadOne(
			p.Person,
			false,
		)
		if err != nil {
			return nil, err
		}
		payloads["field_definition"] = payload
	}

	return payloads, nil
}
