package pco

import (
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
)

var (
	_ jsonapi.Unmarshalable = &List{}
	_ jsonapi.Marshalable   = &List{}
)

func (*List) GetType() string {
	return "List"
}

func (l *List) SetId(s string) error {
	if s != "" {
		l.Id = String(s)
	}
	return nil
}

func (l *List) GetId() (*string, error) {
	return l.Id, nil
}

func (l *List) relations() []relationProperty {
	return []relationProperty{{
		many:    true,
		key:     "people",
		models:  &l.People,
		present: len(l.People) > 0,
	}}
}

func (l *List) SetRelationships(
	doc *jsonapi.Document,
	docMap *jsonapi.IncludedMap,
) error {
	return setRelationships(doc, docMap, l.relations())
}

func (l *List) GetRelationships() (map[string]*jsonapi.Payload, error) {
	payloads := map[string]*jsonapi.Payload{}
	if err := getRelationships(payloads, l.relations()); err != nil {
		return nil, err
	}
	return payloads, nil
}
func (l *List) PeopleIds() []string {
	if l.People == nil {
		return nil
	}

	ids := []string{}
	for _, p := range l.People {
		ids = append(ids, *p.Id)
	}

	return ids
}
