package pco

func String(str string) *string {
	return &str
}

func Bool(b bool) *bool {
	return &b
}
