package pco

import (
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
)

var (
	_ jsonapi.Unmarshalable = &FieldDatum{}
	_ jsonapi.Marshalable   = &FieldDatum{}
)

func (*FieldDatum) GetType() string {
	return "FieldDatum"
}

func (f *FieldDatum) SetId(s string) error {
	if s != "" {
		f.Id = String(s)
	}
	return nil
}

func (f *FieldDatum) GetId() (*string, error) {
	return f.Id, nil
}

func (f *FieldDatum) relations() []relationProperty {
	return []relationProperty{{
		many:    false,
		key:     "field_definition",
		field:   "FieldDefinition",
		models:  f,
		present: f.FieldDefinition != nil,
	}, {
		many:    false,
		key:     "customizable",
		field:   "Customizable",
		models:  f,
		present: f.Customizable != nil,
	}}
}

func (f *FieldDatum) SetRelationships(
	doc *jsonapi.Document,
	docMap *jsonapi.IncludedMap,
) error {
	return setRelationships(doc, docMap, f.relations())
}

func (f *FieldDatum) GetRelationships() (map[string]*jsonapi.Payload, error) {
	payloads := map[string]*jsonapi.Payload{}

	if err := getRelationships(payloads, f.relations()); err != nil {
		return nil, err
	}

	return payloads, nil
}
