package pco

import (
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
)

var (
	_ jsonapi.Unmarshalable = &App{}
	_ jsonapi.Marshalable   = &App{}
)

func (a *App) GetType() string {
	return "App"
}

func (a *App) SetId(s string) error {
	if s != "" {
		a.Id = String(s)
	}

	return nil
}

func (a *App) GetId() (*string, error) {
	return a.Id, nil
}

func (a *App) SetRelationships(
	doc *jsonapi.Document,
	docMap *jsonapi.IncludedMap,
) error {
	return nil
}

func (a *App) GetRelationships() (map[string]*jsonapi.Payload, error) {
	payloads := map[string]*jsonapi.Payload{}
	return payloads, nil
}
