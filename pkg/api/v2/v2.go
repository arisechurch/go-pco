package v2

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/go-querystring/query"
	"gitlab.com/arisechurch/go-jsonapi/pkg/jsonapi"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	BaseUrl = "https://api.planningcenteronline.com"

	Get    = "GET"
	Post   = "POST"
	Patch  = "PATCH"
	Delete = "DELETE"
)

type Client struct {
	httpClient *http.Client

	BaseUrl   string
	AppId     string
	AppSecret string
}

func New(appId, appSecret string) *Client {
	client := &Client{
		httpClient: &http.Client{
			Timeout: 60 * time.Second,
		},
		AppId:     appId,
		AppSecret: appSecret,
		BaseUrl:   BaseUrl,
	}

	return client
}

func (c *Client) makeUrl(app, path string) string {
	url := bytes.Buffer{}
	url.WriteString(c.BaseUrl)
	url.WriteString("/")
	url.WriteString(app)
	url.WriteString("/v2")
	url.WriteString(path)

	return url.String()
}

func (c *Client) makeRequest(
	method,
	app,
	path string,
	body io.Reader,
	opt interface{},
) (*http.Request, error) {
	url := c.makeUrl(app, path)

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	if opt != nil {
		qs, err := query.Values(opt)
		if err != nil {
			return nil, err
		}
		req.URL.RawQuery = qs.Encode()
	}

	req.SetBasicAuth(c.AppId, c.AppSecret)

	return req, nil
}

func (c *Client) Do(req *http.Request) (*http.Response, error) {
	res, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode >= 400 {
		defer res.Body.Close()

		if res.StatusCode == 400 {
			body, _ := ioutil.ReadAll(res.Body)
			fmt.Println(
				req.URL.String(),
				string(body),
			)
		}

		return res, errors.New(fmt.Sprintf(
			"http error: %d",
			res.StatusCode,
		))
	}

	return res, nil
}

func (c *Client) requestWithoutBody(
	method,
	app,
	path string,
	opt interface{},
) (*jsonapi.Payload, *http.Response, error) {
	req, err := c.makeRequest(method, app, path, nil, opt)
	if err != nil {
		return nil, nil, err
	}

	res, err := c.Do(req)
	if err != nil {
		// Retry from rate limiting
		if res != nil && res.StatusCode == 429 {
			time.Sleep(time.Second * 20)
			return c.requestWithoutBody(method, app, path, opt)
		}

		return nil, res, err
	}

	if res.StatusCode == http.StatusNoContent {
		return nil, res, nil
	}

	payload := &jsonapi.Payload{}
	if err := json.NewDecoder(res.Body).Decode(payload); err != nil {
		return nil, res, err
	}

	return payload, res, nil
}

func (c *Client) requestWithBody(
	method,
	app,
	path string,
	model jsonapi.Marshalable,
	opt interface{},
) (*jsonapi.Payload, *http.Response, error) {
	body := &bytes.Buffer{}
	bodyPayload, err := jsonapi.NewPayloadOne(
		model,
		true,
	)
	if err != nil {
		return nil, nil, err
	}

	if err := json.NewEncoder(body).Encode(bodyPayload); err != nil {
		return nil, nil, err
	}

	bodyStr := body.String()

	req, err := c.makeRequest(method, app, path, body, opt)
	if err != nil {
		return nil, nil, err
	}

	res, err := c.Do(req)
	if err != nil {
		// Retry from rate limiting
		if res != nil && res.StatusCode == 429 {
			time.Sleep(time.Second * 20)
			return c.requestWithBody(method, app, path, model, opt)
		} else if res != nil && res.StatusCode == 400 {
			fmt.Println(bodyStr)
		}

		return nil, res, err
	}

	if res.StatusCode == http.StatusNoContent {
		return nil, res, nil
	}

	payload := &jsonapi.Payload{}
	if err := json.NewDecoder(res.Body).Decode(payload); err != nil {
		return nil, res, err
	}

	return payload, res, nil
}

func (c *Client) Get(
	app,
	path string,
	opt interface{},
) (*http.Request, error) {
	return c.makeRequest(Get, app, path, nil, opt)
}

func (c *Client) GetDo(
	app,
	path string,
	opt interface{},
) (*jsonapi.Payload, *http.Response, error) {
	return c.requestWithoutBody(Get, app, path, opt)
}

func (c *Client) GetOne(
	app,
	path string,
	opt interface{},
	model jsonapi.Unmarshalable,
) (*http.Response, error) {
	payload, res, err := c.GetDo(app, path, opt)
	if err != nil {
		return res, err
	}

	if err := payload.UnmarshalOneModel(model); err != nil {
		return res, err
	}

	return res, nil
}

func (c *Client) GetMany(
	app,
	path string,
	opt interface{},
	models interface{},
) (*http.Response, error) {
	payload, res, err := c.GetDo(app, path, opt)
	if err != nil {
		return res, err
	}

	if err := payload.UnmarshalManyModels(models); err != nil {
		return res, err
	}

	return res, nil
}

func (c *Client) Post(
	app,
	path string,
	body io.Reader,
	opt interface{},
) (*http.Request, error) {
	return c.makeRequest(Post, app, path, body, opt)
}

func (c *Client) PostDo(
	app,
	path string,
	model jsonapi.Marshalable,
	opt interface{},
) (*jsonapi.Payload, *http.Response, error) {
	return c.requestWithBody(Post, app, path, model, opt)
}

func (c *Client) Create(
	app,
	path string,
	model jsonapi.Marshalable,
	result jsonapi.Unmarshalable,
	opt interface{},
) (*http.Response, error) {
	payload, res, err := c.PostDo(
		app,
		path,
		model,
		opt,
	)
	if err != nil || result == nil {
		return res, err
	}

	err = payload.UnmarshalOneModel(result)
	return res, err
}

func (c *Client) Patch(
	app,
	path string,
	body io.Reader,
	opt interface{},
) (*http.Request, error) {
	return c.makeRequest(Patch, app, path, body, opt)
}

func (c *Client) PatchDo(
	app,
	path string,
	model jsonapi.Marshalable,
	opt interface{},
) (*jsonapi.Payload, *http.Response, error) {
	return c.requestWithBody(Patch, app, path, model, opt)
}

func (c *Client) Update(
	app,
	path string,
	model jsonapi.Marshalable,
	result jsonapi.Unmarshalable,
	opt interface{},
) (*http.Response, error) {
	payload, res, err := c.PatchDo(
		app,
		path,
		model,
		opt,
	)
	if err != nil {
		return nil, err
	} else if result == nil {
		return res, nil
	}

	err = payload.UnmarshalOneModel(result)
	return res, err
}

func (c *Client) Delete(
	app,
	path string,
	body io.Reader,
	opt interface{},
) (*http.Request, error) {
	return c.makeRequest(Delete, app, path, body, opt)
}

func (c *Client) Remove(
	app,
	path string,
	opt interface{},
) (*http.Response, error) {
	_, res, err := c.requestWithoutBody(Delete, app, path, opt)
	return res, err
}
