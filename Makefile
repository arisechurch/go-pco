test:
	go test ./pkg/...

godep-save:
	rm -rf Godeps/ vendor/ && godep save ./pkg/...

.PHONY: test godep-save
